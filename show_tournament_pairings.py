import requests

URL="https://play.limitlesstcg.com/api"
API_KEY=""
TOURNAMENT=""

headers = {"X-Access-Key" : API_KEY}
r = requests.get(f'{URL}/tournaments/{TOURNAMENT}/pairings', headers=headers)

print(r.json())
