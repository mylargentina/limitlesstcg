# limitlesstcg

Este proyecto integra la plataforma limitless usada por myl argentina con estadisticas internas para poder sacar estadisticas sobre los torneos jugados

## Requisitos

- git
- linux
- docker

## Uso

```sh
# Clonar el repositorio
git clone git@gitlab.com:mylargentina/limitlesstcg.git

# Ingresar al directorio principal
cd limitlesstcg

# Ejecutar el script de inicio
./start_notebook
```

Luego, podrás acceder al código desde  http://localhost:8889
Si el sitio requiere un token, ingresar `my-token`

### Agregar datos de un nuevo torneo

En construcción
